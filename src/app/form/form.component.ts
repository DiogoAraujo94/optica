import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
    productForm: FormGroup;
    tiposAro: any = ['Aro1', 'Aro2', 'Aro3', 'Aro4', 'Aro5'];


    constructor(private _formBuilder: FormBuilder,
                private httpClient: HttpClient) {
        this.productForm = this._formBuilder.group({
            tel: '',
            tipoAro: '',
            x: '',
            y: '',
        });
    }

    ngOnInit(): void {
        console.log('aaa');
    }


    handleGenero(tipo: any): void {
        this.productForm.patchValue({tipoAro: tipo});
    }

    saveProposal(): Promise<any> {
        const data = this.productForm.getRawValue();
        return new Promise((resolve, reject) => {
            const headers = {
                Authorization: 'Bearer my-token',
                X: data.x,
                Y: data.y,
            };
            const body = {
                X: data.x,
                Y: data.y
            };
            console.log('headers', headers);
            this.httpClient.post('link', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}

